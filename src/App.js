import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ComponentC from './components/ComponentC';
import { UserProvider } from './components/UserContext';


class App extends Component {
  render() {
    return (
      <div className="App">
        <UserProvider value ="Rohan">
          <ComponentC />
        </UserProvider>
      </div>
    );
  }
}
export default App;





// import ClickCounter2 from './components/ClickCounter2';
// import HoverCounter2 from './components/HoverCounter2';
// import HOurCounter from './components/HOurCounter';
// import User from './components/User';
// import Counter2 from './components/Counter2';

// <Counter2
//           render={(count, incrementCounter) => (
//             <ClickCounter2 count={count} incrementCounter={incrementCounter} />
//           )} />

//         <Counter2
//           render={(count, incrementCounter) => (
//             <HoverCounter2 count={count} incrementCounter={incrementCounter} />
//           )} />

//          <ClickCounter2 /> 
//         <HoverCounter2 />
//         <User render = {(isLoggedIn)=> isLoggedIn ? 'ROHAN' : 'Guest'}/> 



// import ClickCounter from './components/ClickCounter';
// import HOurCounter from './components/HOurCounter';
// <ClickCounter />
// <HOurCounter />

// import Hero from './components/Hero';
// import ErrorBoundary from './components/ErrorBoundary';
// <ErrorBoundary>
// <Hero heroName='BatMan' />
// </ErrorBoundary>
// <ErrorBoundary>
// <Hero heroName='SuperMan' />
// </ErrorBoundary>
// <ErrorBoundary>
// <Hero heroName='Joker' />
// </ErrorBoundary>


// import PortalsDemo from './components/PortalsDemo';
// <PortalsDemo />

// import PureComp from './components/PureComp';
// import ParentComp from './components/ParentComp';
// import RefsDemo from './components/RefsDemo';
// import FocusInput from './components/FocusInput';
// import FRParentInput from './components/FRParentInput';

// <FRParentInput />
// <FocusInput />
// {/* <RefsDemo /> */}
//   {/* <PureComp /> */}
//   {/* <ParentComp /> */}




// import FragmentDemo from './components/FragmentDemo';
// import Table from './components/Table';
//  {/* <FragmentDemo /> */}
//  <Table />

// import Form from './components/Form';
// import LifeCycleA from './components/LifeCycleA';
// {/* <Form /> */}
// <LifeCycleA />

// import Greet from './components/Greet';
// import Welcome from './components/Welcome';
// import Hello from './components/Hello';
// import Message from './components/Message';
// import Counter from './components/Counter';
// import FunctionClick from './components/FunctionClick';
// import ClassClik from './components/ClassClik';
// import EventBind from './components/EventBind';
// import ParentComponent from './components/ParentComponent';
// import UserGreeting from './components/UserGreeting';
// import NameList from './components/NameList';
// import Stylesheet from './components/Stylesheet';
// import Inline from './components/Inline';
// import './appStyle.css';
// import styles from './appStyle.module.css';
// import Form from './components/Form'


{/* <h1 className='error'>ERROR regular css file</h1>  */ }
{/* <h1 className={styles.success}>SUCCESS module css file</h1> */ }
{/* <Stylesheet primary={false} /> */ }
{/* <Inline /> */ }
{/* <NameList /> */ }
{/* <UserGreeting /> */ }
{/* <ParentComponent /> */ }
{/* <EventBind /> */ }
{/* <FunctionClick />
        <ClassClik /> */}

{/* <Counter /> */ }
{/* <Greet name='Rohan' relation='First name'>
          <p>This is the paragraph of the Rohan.</p>
        </Greet>
        <Greet name='Somnath' relation='Father name' >
        <button>CLICK</button>
        </Greet>
        <Greet name='Sanap' relation='Last name' />
        <Welcome name='Rohan' relation='First name'/>
        <Welcome name='Somnath' relation='Father name' />
        <Welcome name='Sanap' relation='Last name' /> */}
{/* <Hello /> */ }
{/* <Message /> */ }

