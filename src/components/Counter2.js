import React, { Component } from 'react'
import ClickCounter2 from './ClickCounter2'

class Counter2 extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             count : 0
        }
    }
    incremnetCounter = () => {
        this.setState (prevState =>{
            return {count : prevState.count + 1}
        })
    }
    render() {
        return (
            <div>
                {this.props.render(this.state.count, this.incremnetCounter)}
            </div>
        )
    }
}

export default Counter2
