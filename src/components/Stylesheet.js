import React from 'react'
import './myStyle.css'

function Stylesheet(props) {
    let className = props.primary ? 'primary' : 'secondary'
    return (
        <div>
            <h1 className={`${className} fontStyle`}>Stylesheet with orange colour</h1>
        </div>
    )
}

export default Stylesheet