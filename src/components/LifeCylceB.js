import React, { Component } from 'react'

class LifeCycleB extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             name :'Rohan'
        }
        console.log('INSIDE LIFECYCLE_B CONSTRUCTOR')
    }
    
    static getDerivedStateFromProps(props,state){
        console.log('INSIDE LIFECYCLE_B GET_DERIVED_STATE_FROM_PROPS')
        return null;
    }
    componentDidMount() {
        console.log('INSIDE LIFECYCLE_B COMPONENT_DID_MOUNT')
    }

    // Updating lifecyle method
    shouldComponentUpdate() {
        console.log('INSIDE LIFECYCLE_B SHOULD_COMPONENT_UPDATE')
        return true;
    }
    getSnapshotBeforeUpdate(prevProps, prevState){
        console.log('INSIDE LIFECYCLE_B GET_SNAPSHOP_BEFORE_UPDATE')
        return null;
    }
    componentDidUpdate(){
        console.log('INSIDE LIFECYCLE_B COMPONENT_DID_UPDATE')
    }
    render() {
        console.log('INSIDE LIFECYCLE_B RENDER METHOD')
        return (
            
            <div>
                LifeCycle B
            </div>
        )
    }
}

export default LifeCycleB
