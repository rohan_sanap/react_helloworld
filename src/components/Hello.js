import React from "react";

const Hello = () => {
    // return (                         With using JSX
    //     <div>
    //     <h1>Hello Rohan</h1>
    //     </div>
    // )
    return React.createElement(
        'div',                                                   // tag
        { id: 'hello', className: 'dummyClass'},                 // Optional field , it could be null
        React.createElement('h1',null,'Hello Rohan Without JSX'))// Value/ nested method
}

export default Hello