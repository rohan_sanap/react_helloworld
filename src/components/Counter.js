import React, { Component } from 'react'

class Counter extends Component {

    constructor(props) {
        super(props)

        this.state = {
            count: 0
        }
    }
    increase() {
        this.setState({
            count: this.state.count + 1
        },
        () => {
                console.log('Callback value : ',this.state.count)
            }

        )
    }
    increaseFive(){
        for (let i =0; i< 5; i++){
            this.increase()
        }
    }
    reset(){
        this.setState({
            count: 0
        },
        () =>{
            console.log('Call back reset value : ',this.state.count)
        })
    }
    render() {
        return (
            <div>
                <div>Count - {this.state.count}</div>
                <button onClick={() => this.increaseFive()}>Increment</button>
                <button onClick={() => this.reset()}>RESET</button>
            </div>
        )
    }
}

export default Counter
