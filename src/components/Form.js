import React, { Component } from 'react'

class Form extends Component {
    constructor(props) {
        super(props)

        this.state = {
            userName: '',
            comments: '',
            gender : 'male'
        }
    }
    handleUserNameChange = (event) => {
        this.setState({
            userName: event.target.value
        }
        )
    }
    handleComment = (event) =>{
        this.setState({
            comments: event.target.value
        }
        )
    }
    selectChangehandler = (event) =>{
        this.setState({
            gender : event.target.value
        }
            
        )
    }
    handleSubmit =(event) =>{
        alert(`User ${this.state.userName} having ${this.state.comments} submitted the data with gender ${this.state.gender}`)
        event.preventDefault()
    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <div>
                    <label>UserName : </label>
                    <input type='text' value={this.state.userName} onChange={this.handleUserNameChange} />
                </div>
                <div>
                    <label>Comments : </label>
                    <textarea value={this.state.comments} onChange ={this.handleComment}></textarea>
                </div>
                <div>
                    <label>Select the gender :</label>
                    <select value={this.state.gender} onChange={this.selectChangehandler}>
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="others">Others</option>
                    </select>
                </div>
                <div>
                    <button type='submit'>SUBMIT</button>
                </div>
            </form>
        )
    }
}

export default Form
