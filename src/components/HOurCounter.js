import React, { Component } from 'react'
import UpdatedComponent from './withCounter'

class HOurCounter extends Component {
    
    render() {
        
        const { count, incremenetCount} = this.props
        return (
        <h2 onMouseOver ={incremenetCount}>Hour {count} time</h2>                
        )
    }
}

export default UpdatedComponent(HOurCounter)
