import React, { Component } from 'react'
import FRInput from './FRInput'

class FRParentInput extends Component {
    constructor(props) {
        super(props)
    
        this.parentref = React.createRef()
    }
    clickHandler = () =>{
        this.parentref.current.focus()
    }
    
    render() {
        return (
            <div>
                <FRInput ref={this.parentref} />
                <button onClick={this.clickHandler}>Focus Input</button>
            </div>
        )
    }
}

export default FRParentInput
