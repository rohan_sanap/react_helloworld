import React from 'react'
import PersonList from './PersonList'

function NameList() {
    let names = ['rohan', 'ram', 'ajay', 'sham']
    const persons = [
        {
            id: 1,
            name: 'Bruce',
            age: 30,
            skill: 'React'
        },
        {
            id: 2,
            name: 'Clark',
            age: 25,
            skill: 'Angular'
        },
        {
            id: 3,
            name: 'Diana',
            age: 28,
            skill: 'Vue'
        }
    ]

    // const personList = persons.map(person => (
    //     <PersonList key={person.id} person={person}></PersonList>
    // )
    // )
    const nameList = names.map( (name,index) => <h2 key={index}> {name} with index {index}</h2>)
    return <div>{nameList}</div>
    // return (
    //     <div>{personList}</div>
    // )
}

export default NameList
