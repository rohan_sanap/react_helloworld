import React, { Component } from 'react'

class EventBind extends Component {

    constructor(props) {
        super(props)

        this.state = {
            message: 'Hello'
        }
        // this.clickable = this.clickable.bind(this) bindin from constructor
    }

    // clickable() {
        
    //     this.setState({
    //         message: 'GoodBye'
    //     })
    // }

    clickable = () =>{
        this.setState({
            message: 'GoodBye from arrow function'
        })
    }
    render() {
        return (
            <div>
                <div>{this.state.message}</div>
                {/* <button onClick={this.clickable.bind(this)}>CLICK</button> // binding by using bind in render() */}
                {/* <button onClick ={() => this.clickable()}>CLICK</button> // binding using arrow function */}
                <button onClick={this.clickable}>CLICK</button>
            </div>
        )
    }
}

export default EventBind
