import React, { Component } from 'react'
import LifeCycleB from './LifeCylceB'

class LifeCycleA extends Component {
    // Mouning lifecyle methods
    constructor(props) {
        super(props)

        this.state = {
            name: 'Rohan'
        }
        console.log('INSIDE LIFECYCLE_A CONSTRUCTOR')
    }

    static getDerivedStateFromProps(props, state) {
        console.log('INSIDE LIFECYCLE_A GET_DERIVED_STATE_FROM_PROPS')
        return null;
    }
    componentDidMount() {
        console.log('INSIDE LIFECYCLE_A COMPONENT_DID_MOUNT')
    }

    // Updating lifecyle method
    shouldComponentUpdate() {
        console.log('INSIDE LIFECYCLE_A SHOULD_COMPONENT_UPDATE')
        return true;
    }
    getSnapshotBeforeUpdate(prevProps, prevState){
        console.log('INSIDE LIFECYCLE_A GET_SNAPSHOP_BEFORE_UPDATE')
        return null;
    }
    componentDidUpdate(){
        console.log('INSIDE LIFECYCLE_A COMPONENT_DID_UPDATE')
    }

    changeState = () =>{
        this.setState ({
            name : 'CODEVALUATION'
        })
    }
    render() {
        console.log('INSIDE LIFECYCLE_A RENDER METHOD')
        return (
            <div>
                <div>
                    LifeCycle A
                </div>
                <button onClick= {this.changeState}>Change State</button>
                <LifeCycleB />
            </div>
        )
    }
}

export default LifeCycleA
