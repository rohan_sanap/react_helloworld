import React, { Component } from 'react'

export class ClassClik extends Component {


    clickHandler(){
        console.log('Class click clicked....!!')
    }
    render() {
        return (
            
            <div>
                <button onClick = {this.clickHandler}>Class Click</button>
            </div>
        )
    }
}

export default ClassClik
