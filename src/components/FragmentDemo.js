import React from 'react'

function FragmentDemo() {
    return (
        <React.Fragment>
            <h1>
                Fragment demo
            </h1>
            <p>
                This descibes the Fragment Component demo
            </p>
        </React.Fragment>
    )
}

export default FragmentDemo
