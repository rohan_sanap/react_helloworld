import React, {Component} from "react";

class Welcome extends Component{

render(){
    return <h1>Class Component with the values {this.props.name} as {this.props.relation}</h1>
}
}
export default Welcome