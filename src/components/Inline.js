import React from 'react'



function Inline() {
    const heading = {
        fontSize : '80px',
        color : 'blue'
    }
    return (
        <div>
            <h1 style={heading}>INLINE</h1>
        </div>
    )
}

export default Inline
