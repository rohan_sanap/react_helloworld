import React, { Component } from 'react'
import UpdatedComponent from './withCounter'

 class ClickCounter extends Component {
     
    render() {
                const {count, incremenetCount}= this.props

                return <button onClick={incremenetCount}>Clicked {count} times</button>
    }
}

export default UpdatedComponent(ClickCounter)
